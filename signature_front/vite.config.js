import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ],
  optimizeDeps: {
    include: [
      '@rushstack/eslint-patch',
      '@vitejs/plugin-vue',
      'element-plus',
      'lodash-es',
      'vue',
      'vue-router',
      'pinia',
    ],
  },
  server: {
    host: true,
    origin: 'http://127.0.0.1:8080',
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  template: {
    compilerOptions: {
      isCustomElement: (tag) => tag.includes('-')
    }
  }
})
