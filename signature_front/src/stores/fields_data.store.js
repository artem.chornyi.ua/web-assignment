import { defineStore } from 'pinia';

export const useFieldsDataStore = defineStore({
    id: 'fields',
    state: () => ({
        fields_data: {
            fullname: '',
            title: '',
            company: '',
            phone: '',
            email: '',
            address: ''
        }
    }),
    actions: {
        update(new_data) {
            // update pinia state
            for (const key of Object.keys(new_data)) {
                this.fields_data[key] = new_data[key];
            }

            // store signature fields in local storage
            localStorage.setItem('fields', JSON.stringify(this.fields_data));
        },
        get() {
            return localStorage.getItem('fields', JSON.parse(this.fields_data));
        }
    }
});
