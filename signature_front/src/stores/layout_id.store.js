import { defineStore } from 'pinia';

export const useLayoutIDStore = defineStore({
    id: 'lay',
    state: () => ({
        layout_id: 1
    }),
    actions: {
        update(new_lay_id) {
            // store layout_id in local storage
            localStorage.setItem('lay', JSON.stringify(new_lay_id));

            // update pinia state
            this.layout_id = new_lay_id;
        }
    }
});
