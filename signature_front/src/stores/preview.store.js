import { defineStore } from 'pinia';

export const usePreviewStore = defineStore({
    id: 'preview',
    state: () => ({
        html: ''
    }),
    actions: {
        update(new_html) {
            // store signature fields in local storage
            localStorage.setItem('preview', JSON.stringify(new_html));

            // update pinia state
            this.html = new_html;
        }
    }
});
