import { defineStore } from 'pinia';
import router from '../router';

export const useAuthStore = defineStore({
    id: 'auth',
    state: () => ({
        // initialize state from local storage to enable user to stay logged in
        user: JSON.parse(localStorage.getItem('user')),
        returnUrl: null
    }),
    actions: {
        login(user) {
            // store user details in local storage to keep user logged in between page refreshes
            localStorage.setItem('user', JSON.stringify(user));

            // update pinia state
            this.user = user;

            // redirect to previous url or default to home page
            router.push(this.returnUrl || '/');
        },
        logout() {
            this.user = null;
            localStorage.clear();
            router.push('/login');
        }
    }
});
