import { defineStore } from 'pinia';

export const useLastSignatureStore = defineStore({
    id: 'last_signature',
    state: () => ({
        signatureObject: {}
    }),
    actions: {
        update(new_signature) {
            // store signature in local storage
            localStorage.setItem('last_signature', JSON.stringify(new_signature));

            // update pinia state
            this.signatureObject = new_signature;
        },
        get() {
            return localStorage.getItem('last_signature', JSON.parse(this.signatureObject));
        }
    }
});
