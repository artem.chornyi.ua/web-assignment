import { createApp } from 'vue';
import { createPinia } from 'pinia';

import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import App from './App.vue';
import router from './router';
import axios from 'axios';

import './assets/main.css';

const axiosInstance = axios.create({
    withCredentials: true
});

const app = createApp(App);

app.config.globalProperties.$axios = { ...axiosInstance };

app.use(ElementPlus);
app.use(createPinia());
app.use(router);

app.mount('#app');
