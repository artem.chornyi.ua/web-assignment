import json

from fastapi import Depends, FastAPI, HTTPException, Request, status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session

import crud
import models
import schemas
from back_exceptions import update_exc_format
from db_config import SessionLocal, engine
from layouts_generator import text_layouts_generator

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:5173",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# DB dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


# Customization of validation exception
@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    formated_exc = jsonable_encoder(update_exc_format(json.loads(exc.json())))
    return JSONResponse(content=formated_exc, status_code=status.HTTP_400_BAD_REQUEST)


# Fill DB with layouts on startup if needed
@app.on_event("startup")
def fill_default_layouts(db: Session = SessionLocal()):
    for file_name, text in text_layouts_generator:
        layout = crud.get_layout_by_name(db=db, name=file_name)
        if layout is None:
            crud.create_layout(db=db, name=file_name, html_structure=text)


# API endpoints
@app.post("/users/", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        return db_user
    return crud.create_user(db=db, user=user)


@app.get("/users/{user_id}", response_model=schemas.User)
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.post("/users/{user_id}/signatures/", response_model=schemas.Signature)
def create_signature_for_user(
    user_id: int, signature: schemas.SignatureCreate, db: Session = Depends(get_db)
):
    return crud.create_user_signature(db=db, signature=signature, user_id=user_id)


@app.get("/users/{user_id}/signatures/", response_model=list[schemas.Signature])
def read_all_user_signatures(user_id: int, db: Session = Depends(get_db)):
    return crud.get_user_signatures(db, user_id=user_id)


@app.get("/users/{user_id}/signatures/{signature_id}", response_model=schemas.Signature)
def read_user_signature(user_id: int, signature_id: int, db: Session = Depends(get_db)):
    signature = crud.get_signature(db, user_id=user_id, signature_id=signature_id)
    if signature is None:
        raise HTTPException(status_code=404, detail="Signature not found")
    return signature


@app.get("/users/{user_id}/layouts/{signature_id}", response_model=schemas.FilledLayout)
def read_stored_user_layout(
    user_id: int, signature_id: int, db: Session = Depends(get_db)
):
    orm_signature = crud.get_signature(db, user_id=user_id, signature_id=signature_id)
    if orm_signature is None:
        raise HTTPException(status_code=404, detail="Signature not found")
    signature = schemas.Signature.from_orm(orm_signature)
    filled_layout = crud.get_user_layout(db=db, signature=signature)
    return filled_layout


@app.post("/layouts_preview/", response_model=schemas.FilledLayout)
def create_preview_user_layout(
    signature: schemas.SignatureCreate, db: Session = Depends(get_db)
):
    return crud.get_user_layout(db=db, signature=signature)


@app.get("/layouts/", response_model=list[schemas.Layout])
def read_all_layouts(db: Session = Depends(get_db)):
    layouts = crud.get_layouts(db)
    return layouts


@app.get("/layouts/{layout_id}", response_model=schemas.Layout)
def read_layout(layout_id: int, db: Session = Depends(get_db)):
    db_layout = crud.get_layout(db, layout_id=layout_id)
    if db_layout is None:
        raise HTTPException(status_code=404, detail="Layout not found")
    return db_layout
