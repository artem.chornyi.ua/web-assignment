import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import crud
from db_config import Base
from layouts_generator import text_layouts_generator
from main import app, get_db

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


@pytest.fixture()
def test_db():
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)


@pytest.fixture()
def test_layouts(db=TestingSessionLocal()):
    for file_name, text in text_layouts_generator:
        layout = crud.get_layout_by_name(db=db, name=file_name)
        if layout is None:
            crud.create_layout(db=db, name=file_name, html_structure=text)


def test_create_user(test_db):
    response = client.post(
        "/users/",
        json={"email": "deadpool@example.com"},
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "deadpool@example.com"
    assert "id" in data
    user_id = data["id"]

    # test_read_user
    response = client.get(f"/users/{user_id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "deadpool@example.com"
    assert data["id"] == user_id

    # test 400 exception format
    fields_data = {
        "fullname": "",
        "title": "Job title",
        "company": "company",
        "email": "",
        "phone": "+972541234567",
        "address": "My office address",
        "layout_id": 1,
    }
    response = client.post(
        f"/users/{user_id}/signatures/",
        json=fields_data,
    )
    assert response.status_code == 400, response.text
    data = response.json()
    assert data["errors"][0]["code"] == "REQUIRED_FIELD"
    assert data["errors"][1]["code"] == "INVALID_FIELD_FORMAT"


def test_create_signature_for_user(test_db, test_layouts):
    user_id = 1
    fields_data = {
        "fullname": "Full Name",
        "title": "Job title",
        "company": "company",
        "email": "address@email.com",
        "phone": "+972541234567",
        "address": "My office address",
        "layout_id": 1,
    }
    response = client.post(
        f"/users/{user_id}/signatures/",
        json=fields_data,
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "address@email.com"
    assert "id" in data
    signature_id = data["id"]

    # test_read_user_signature
    response = client.get(f"/users/{user_id}/signatures/{signature_id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "address@email.com"
    assert data["id"] == signature_id

    # test_read_all_user_signatures
    response = client.get(f"/users/{user_id}/signatures/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data[0]["email"] == "address@email.com"
    assert len(data) == 1

    # test_read_stored_user_layout
    response = client.get(f"/users/{user_id}/layouts/{signature_id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["html"].startswith("<!DOCTYPE html>")
    assert "address@email.com" in data["html"]

    # test_read_all_layouts
    response = client.get("/layouts/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert len(data) == 2
    assert data[0]["name"] == "column"
    assert data[1]["name"] == "horizontal"

    # test_read_layout
    layout_id = 1
    response = client.get(f"/layouts/{layout_id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["name"] == "column"
    assert data["html_structure"].startswith("<!DOCTYPE html>")

    # test_create_preview_user_layout
    response = client.post(
        "/layouts_preview/",
        json=fields_data,
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["html"].startswith("<!DOCTYPE html>")
    assert "address@email.com" in data["html"]
