FULLNAME_TAG = "<signature-fullname>Full Name</signature-fullname>"
TITLE_TAG = "<signature-title>Job title</signature-title>"
COMPANY_TAG = "<signature-company>company</signature-company>"
PHONE_TAG = "<signature-phone>+972541234567</signature-phone>"
EMAIL_TAG = "<signature-email>address@email.com</signature-email>"
ADDRESS_TAG = "<signature-address>My office address</signature-address>"

TAGS_DICT = {
    "fullname": FULLNAME_TAG,
    "title": TITLE_TAG,
    "company": COMPANY_TAG,
    "email": EMAIL_TAG,
    "phone": PHONE_TAG,
    "address": ADDRESS_TAG,
}


def replace_tags(text: str, fields_data: dict, tags: dict = TAGS_DICT) -> str:
    for key, value in tags.items():
        text = text.replace(value, fields_data[key])
    return text
