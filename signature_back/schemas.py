from datetime import datetime

from pydantic import BaseModel, validate_email, validator

from back_exceptions import InvalidFieldFormat, RequiredField

DOCTYPE = "<!DOCTYPE html>"


def check_content_doctype(html_text: str):
    if DOCTYPE not in html_text:
        raise ValueError("Should be valid html format")
    return html_text


def check_empty_string(field_data: str):
    if field_data == "":
        raise RequiredField()
    return field_data


def check_email(email: str):
    try:
        validate_email(email)
    except Exception as e:
        raise InvalidFieldFormat() from e
    return email


class FilledLayout(BaseModel):
    html: str

    # validators
    _doctype_checker = validator("html", allow_reuse=True)(check_content_doctype)


class LayoutBase(BaseModel):
    name: str
    html_structure: str

    # validators
    _doctype_checker = validator("html_structure", allow_reuse=True)(
        check_content_doctype
    )
    _not_empty = validator("*", allow_reuse=True)(check_empty_string)


class LayoutCreate(LayoutBase):
    pass


class Layout(LayoutBase):
    id: int

    class Config:
        orm_mode = True


class SignatureBase(BaseModel):
    fullname: str
    title: str
    company: str
    email: str
    phone: str
    address: str

    # validators
    _not_empty = validator("*", allow_reuse=True)(check_empty_string)
    _check_email_format = validator("email", allow_reuse=True)(check_email)


class SignatureCreate(SignatureBase):
    layout_id: int = 1


class Signature(SignatureCreate):
    id: int
    owner_id: int
    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    email: str

    # validators
    _check_email_format = validator("email", allow_reuse=True)(check_email)


class UserCreate(UserBase):
    pass


class User(UserBase):
    id: int
    signatures: list[Signature] = []

    class Config:
        orm_mode = True
