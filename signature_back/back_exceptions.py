from pydantic.errors import PydanticValueError


class RequiredField(PydanticValueError):
    """Raise when required field is empty"""

    msg_template = "FIELD_NAME is a mandatory field"
    code = "REQUIRED_FIELD"


class InvalidFieldFormat(PydanticValueError):
    """Raise when field has invalid format"""

    msg_template = "FIELD_NAME must be in the correct format"
    code = "INVALID_FIELD_FORMAT"


def reformat_exception_block(block: dict) -> dict:
    code = block["type"].split(".")[-1]
    field = block["loc"][-1]
    message = block["msg"].replace("FIELD_NAME", field.capitalize())
    return {"code": code, "field": field, "message": message}


def update_exc_format(exc: dict) -> dict:
    return {"errors": [reformat_exception_block(block) for block in exc]}
