import glob
import os
import re
from pathlib import Path
from typing import Generator

HTML_EXTENSION = ".html"
UTF_8 = "utf-8"
READ_SHORTCUT = "r"
SLASH = "/"
R_SPACES = r"[\n\t][\s]*"
LAYOUTS_FOLDER = "layouts"


parent_folder_path = os.path.dirname(os.path.abspath(__file__))
layouts_path = Path(parent_folder_path, LAYOUTS_FOLDER)
pattern_list = glob.glob(os.path.join(layouts_path, f"*{HTML_EXTENSION}"))


def get_filename_from_path(path: str, extension: str) -> str:
    filename_with_extension = path.split(SLASH)[-1]
    filename = filename_with_extension.replace(extension, "")
    return filename


def clean_layout(grime: str, layout_text: str) -> str:
    output = re.sub(grime, "", layout_text)
    return output


def generate_text_from_layout(glob_pattern_list: list) -> Generator:
    for filepath in glob_pattern_list:
        with open(filepath, READ_SHORTCUT, encoding=UTF_8) as file:
            file_text = clean_layout(grime=R_SPACES, layout_text=file.read())
            file_name = get_filename_from_path(path=filepath, extension=HTML_EXTENSION)
            yield file_name, file_text


text_layouts_generator = generate_text_from_layout(glob_pattern_list=pattern_list)
