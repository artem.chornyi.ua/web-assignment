from datetime import datetime

from fastapi import HTTPException
from sqlalchemy.orm import Session

import models
import schemas
from layouts_filler import replace_tags


# user functions
def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()


def create_user(db: Session, user: schemas.UserCreate):
    db_user = models.User(email=user.email)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


# layout functions
def get_layout(db: Session, layout_id: int):
    return db.query(models.Layout).filter(models.Layout.id == layout_id).first()


def get_layouts(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Layout).offset(skip).limit(limit).all()


def get_layout_by_name(db: Session, name: str):
    return db.query(models.Layout).filter(models.Layout.name == name).first()


def get_user_layout(db: Session, signature: schemas.SignatureCreate):
    db_layout = get_layout(db, layout_id=signature.layout_id)
    if db_layout is None:
        raise HTTPException(status_code=404, detail="Layout not found")
    layout_text = db_layout.html_structure
    filled_text = replace_tags(text=layout_text, fields_data=signature.dict())
    result_layout = schemas.FilledLayout(html=filled_text)
    return result_layout


def create_layout(db: Session, name: str, html_structure: str):
    db_layout = models.Layout(name=name, html_structure=html_structure)
    db.add(db_layout)
    db.commit()
    db.refresh(db_layout)
    return db_layout


# signature functions
def get_signature(db: Session, user_id: int, signature_id: int):
    return (
        db.query(models.Signature)
        .filter(
            models.Signature.id == signature_id and models.Signature.owner_id == user_id
        )
        .first()
    )


def get_user_signatures(db: Session, user_id: int):
    return db.query(models.Signature).filter(models.Signature.owner_id == user_id).all()


def create_user_signature(
    db: Session, signature: schemas.SignatureCreate, user_id: int
):
    now = datetime.now()
    creation_info = {"created_at": now, "updated_at": now}
    signature_with_creation_info = creation_info | signature.dict()
    db_signature = models.Signature(**signature_with_creation_info, owner_id=user_id)
    db.add(db_signature)
    db.commit()
    db.refresh(db_signature)
    return db_signature
