


## Installation

```console
$ docker compose build
```



### Run it

Run the server with:


```console
$ docker compose up
```


### UI

Go to http://localhost:5173/


### Interactive API docs

Now go to <a href="http://127.0.0.1:8080/docs" class="external-link" target="_blank">http://127.0.0.1:8080/docs.

You will see the automatic interactive API documentation (provided by <a href="https://github.com/swagger-api/swagger-ui" class="external-link" target="_blank">Swagger UI</a>):

![Swagger UI](https://fastapi.tiangolo.com/img/index/index-01-swagger-ui-simple.png)
